
<?php

function get_data($url, $wikipedia = 0) {
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    if ($wikipedia == 1) {
        curl_setopt($ch, CURLOPT_USERAGENT, 'kolin.me.uk random album generator (kolin@kolin.me.uk)');
    }
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function get_album_name($data) {
    $album_DOM = new DOMDocument;
    @$album_DOM->loadHTML($data);
    $album_items = $album_DOM->getElementsByTagName('dt');
    foreach ($album_items as $item) {
        $album_name = $item->textContent;
    }

    $words = explode(' ', $album_name);

    $album_name = "";
    $album_name .= ucfirst($words[count($words) - 6]) . " ";
    $album_name .= $words[count($words) - 5] . " ";
    $album_name .= $words[count($words) - 4] . " ";
    $album_name .= $words[count($words) - 3] . " ";
    $album_name .= $words[count($words) - 2];

    return $album_name;
}

function get_album_art($data) {
    $art_DOM = new DOMDocument;
    @$art_DOM->loadHTML($data);
    $art_items = $art_DOM->getElementsByTagName('img');
    foreach ($art_items as $item) {
        if (strpos($item->getAttribute('src'), 'staticflickr.com')) {
            $art_img = $item->getAttribute('src');
        }
    }
    return $art_img;
}

function get_band_name($data) {
    $band_DOM = new DOMDocument;
    @$band_DOM->loadHTML($data);
    $band_items = $band_DOM->getElementsByTagName('h1');
    foreach ($band_items as $item) {
        $band_name = $item->textContent;
    }

    return $band_name;
}

function get_class_name($band){
    $classes = array('left', 'right');
    if($band < 18){
        $classes[] = 'center';
    }
    return $classes[array_rand($classes)];
}

if(!isset($_GET['save'])){
    $type = "Random: ";
    $data['band'] = get_band_name(get_data('http://en.wikipedia.org/wiki/Special:Random', 1));
    $data['album']= get_album_name(get_data('http://quotationspage.com/random.php3'));
    $data['art'] = get_album_art(get_data('http://www.flickr.com/explore/interesting/7days'));
    @$data['class'] = get_class_name($band);
}else{
    $type = "Saved: ";
    $data = unserialize(base64_decode($_GET['save']));
}

$link = '/album/?save='.base64_encode(serialize($data));

?>

<?php

header("Pragma: no-cache");
header("cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>



<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?=$type?><?=$data['band']?> - <?=$data['album']?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:400,900' rel='stylesheet' type='text/css'>
    <style type="text/css">

        a{
            font-family:"Source Code Pro", sans-serif;
            font-size:15px;
            text-decoration:none;
            font-weight:bold;
            margin-right:328px;
            color:#000;
            display:inline-block;
        }

        a.last{
            margin-right:0px;
        }

        a:hover{
            color:rgb(3, 38, 148);
        }

        #wrap{
            width:600px;
            margin:0px auto;
            margin-top:100px;
        }

        #album{
            width: 600px;
            height: 600px;
            overflow: hidden;
            background: url('<?= $data['art'] ?>') top left no-repeat;
            background-size:cover;
            position:relative;
            margin-bottom:20px;
            margin-top:20px;
        }

        #title{
            position: absolute;
            width:500px;
        }

        #album.left #title{
            margin:50px;
            top:-15px;
        }

        #album.center #title{
            padding:50px;
            margin:0px auto;
            top:150px;
            text-align:center;
        }

        #album.right #title{
            padding:50px;
            margin:0px auto;
            bottom:0px;
            right:0px;
            text-align:right;
        }

        h1, h2{
            font-family:"Source Code Pro", sans-serif;
            font-weight:900;
            color:#fff;
            text-shadow: 1px 0 0 #000, 0 -1px 0 #000, 0 1px 0 #000, -1px 0 0 #000;
        }

        h1{
            font-size:45px;
        }

        h2{
            font-size:25px;
        }


    </style>
</head>
<body>
    <div id="wrap">
        <a href="<?=$link?>">Link to this album</a><a href="/" class="last">Random album</a>
        <div id="album" class="<?=$data['class']?>">
            <div id="title">
                <h1><?= $data['band'] ?></h1>
                <h2><?= $data['album'] ?></h2>
            </div>
        </div>
        <a href="<?=$link?>">Link to this album</a><a href="/" class="last">Random album</a>
    </div>
        <script type="application/javascript">
          (function(b,o,n,g,s,r,c){if(b[s])return;b[s]={};b[s].scriptToken="XzYxNjg2MDg2MA";b[s].callsQueue=[];b[s].api=function(){b[s].callsQueue.push(arguments);};r=o.createElement(n);c=o.getElementsByTagName(n)[0];r.async=1;r.src=g;r.id=s+n;c.parentNode.insertBefore(r,c);})(window,document,"script","//cdn.oribi.io/XzYxNjg2MDg2MA/oribi.js","ORIBI");
        </script>
</body>
</html>
